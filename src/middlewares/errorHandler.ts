import { NextFunction, Request, Response } from 'express';
import { HTTP_STATUS } from '../utils/constants';
import ErrorHandler from '../utils/errorHandler';

export default function (
    err: unknown,
    req: Request,
    res: Response,
    next: NextFunction,
) {
    if (err instanceof ErrorHandler) {
        res.status(err.statusCode).json({
            error: true,
            message: err.message,
        });
    } else {
        res.status(HTTP_STATUS.ERROR).json({
            error: true,
            message: 'Error inesperado',
        });
    }
}
