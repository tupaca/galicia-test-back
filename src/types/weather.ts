export type WeatherNames =
	| 'clear sky'
	| 'few clouds'
	| 'scattered clouds'
	| 'overcast clouds'
	| 'broken clouds'
	| 'shower rain'
	| 'light rain'
	| 'rain'
	| 'thunderstorm'
	| 'snow'
	| 'mist'

export type WeatherResponse = {
	weather: [
		{ id: number; main: string; description: WeatherNames; icon: string }
	]
	main: {
		temp: number
		pressure: number
		humidity: number
	}
	visibility: number
	wind: {
		speed: number
	}
	dt: number
}

export type Weather = {
	country: string
	city: string
	day: string
	weather: string
	temperature: { celsius: number; fahrenheit: number }
	rainProbability: number
	humidity: number
	wind: number
}

export type WeatherParams = {
	country: string
	city: string
}
