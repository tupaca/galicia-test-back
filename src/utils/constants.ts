export const WEATHER = {
    'clear sky': 'Soleado',
    'few clouds': 'Poco nublado',
    'scattered clouds': 'Nubes dispersas',
    'broken clouds': 'Nublado',
    'overcast clouds': 'Nublado',
    'shower rain': 'Llovizna',
    'light rain': 'Llovizna',
    rain: 'LLuvia',
    thunderstorm: 'Tormenta Electrica',
    snow: 'Nieve',
    mist: 'Neblina',
};

export const HTTP_STATUS = {
    OK: 200,
    BAD_REQUEST: 400,
    ERROR: 500,
    NOT_FOUND: 404,
};
