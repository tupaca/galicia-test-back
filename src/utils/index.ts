import { WEATHER } from './constants';
import { WeatherNames } from '../types/weather';

export const getDayByTimeStamp = (timestamp: number): string => {
    const date = new Date(timestamp * 1000);
    const day = date.getDay();
    const days = [
        'Domingo',
        'Lunes',
        'Martes',
        'Miercoles',
        'Jueves',
        'Viernes',
        'Sabado',
    ];
    return days[day];
};
export const getWeather = (weather: WeatherNames): string => WEATHER[weather];
export const celsiusToFahrenheit = (celsius: number): number => (celsius * 9) / 5 + 32;
