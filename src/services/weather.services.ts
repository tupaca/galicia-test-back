import config from '../config';
import request from '../config/axios';
import { Weather, WeatherResponse } from '../types/weather';
import { celsiusToFahrenheit, getDayByTimeStamp, getWeather } from '../utils';

export default class WeatherService {
    private formatWeatherResponse(
        response: WeatherResponse,
        country: string,
        city: string,
    ): Weather {
        
        return {
            country,
            city,
            day: getDayByTimeStamp(response.dt),
            weather: getWeather(response.weather[0].description),
            temperature: {
                celsius: Math.floor(response.main.temp),
                fahrenheit: parseFloat(celsiusToFahrenheit(response.main.temp).toFixed(1)),
            },
            rainProbability: 10,
            humidity: response.main.humidity,
            wind: response.wind.speed,
        };
    }

    async getWeather(city: string, country: string): Promise<Weather> {
        const response = await request.get(
            `/weather?q=${city},${country}&appid=${config.serviceWeather.apiKey}&units=metric`,
        );
        const weatherInfo: Weather = this.formatWeatherResponse(
            response.data,
            country,
            city,
        );
        return weatherInfo;
    }
}
