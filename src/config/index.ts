export default {
    api: {
        port: process.env.PORT || 3000,
    },
    serviceWeather: {
        apiKey: process.env.WEATHER_API_KEY,
        apiUrl: process.env.WEATHER_API_URL,
    },
};
