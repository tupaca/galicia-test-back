import axios, { AxiosInstance } from 'axios';
import config from '.';

const request: AxiosInstance = axios.create({
    baseURL: config.serviceWeather.apiUrl,
    timeout: 5000,
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
    },
});

export default request;
