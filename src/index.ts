import './config/env'
import app from './app'

import config from './config'
app.listen(config.api.port, () => {
	console.log(`Server is running in ${config.api.port}`)
})
