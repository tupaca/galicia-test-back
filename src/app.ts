import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import path from 'path';
import bodyParser from 'body-parser';
import errorHandler from './middlewares/errorHandler';
import indexRoutes from './routes/weather.routes';

const app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(express.static(`${process.cwd()}/public`));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.get('/', (req, res) => {
    res.sendFile(path.join(process.cwd(), 'public', 'index.html'));
});

app.use('/api', indexRoutes);
app.use(errorHandler);

export default app;
