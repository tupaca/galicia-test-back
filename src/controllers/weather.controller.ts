import { NextFunction, Request, Response } from 'express';
import WeatherService from '../services/weather.services';
import { WeatherParams } from '../types/weather';
import { HTTP_STATUS } from '../utils/constants';
import ErrorHandler from '../utils/errorHandler';

const weatherService = new WeatherService();
export default {
    getWeather: async (req: Request, res: Response, next: NextFunction) => {
        const { city, country } = req.query as WeatherParams;
        if (!city || !country) {
            next(
                new ErrorHandler(
                    HTTP_STATUS.BAD_REQUEST,
                    'Por favor ingrese una ciudad y un país',
                ),
            );
            return;
        }
        try {
            const response = await weatherService.getWeather(city, country);

            res.status(HTTP_STATUS.OK).json({ error: false, weather: response });
        } catch (e: any) {
            if (e.response.status === HTTP_STATUS.NOT_FOUND) {
                const error = new ErrorHandler(
                    e.response.status,
                    'No se encontro la ciudad',
                );
                next(error);
            }
            next(e);
        }
    },
};
